<?php 
class Operaciones{
    public $OperacionRealizar = "";

    public function Area($a){
        return $a*$a;
    }
    public function Perimetro($a){
        return $a*4;
    }
    public function Diagonal($a){
        return $a*1.41421356;
    }

    public function ResultadoOperacion($a){

        switch ($this->OperacionRealizar) {
            case 'area':
                return $this->Area($a);
                break;
            case 'perimetro':
                return $this->Perimetro($a);
                break;
            case 'diagonal':
                return $this->Diagonal($a);
                break;
            
            default:
                return "Operacion no definida";
                break;
        }
    }


}



?>