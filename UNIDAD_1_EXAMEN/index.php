<?php 

class consola{

    public $ram = 0;
    public $precio = 0;
    public $almacenamiento = 0;

    public function setram($px_ram){
        $this->ram = $px_ram;
    }

    public function getram(){
        return $this->ram."px";
    }
    public function setprecio($px_precio){
        $this->precio = $px_precio;
    }

    public function getprecio(){
        return $this->precio."px";
    }
    public function setalmacenamiento($px_almacenamiento){
        $this->almacenamiento = $px_almacenamiento;
    }
    public function getalmacenamiento(){
        return $this->almacenamiento."px";

    }
}
$objPS4 = new Consola();

$objPS4->setram(8);

$objPS4->setprecio(399);

$objPS4->setalmacenamiento(500);

 echo "Ram (gb) PS4 :". $objPS4->getram()."<br>";

 echo "Precio (dollar) PS4 :". $objPS4->getprecio()."<br>";

 echo "Almacenamiento (gb) PS4 :". $objPS4->getalmacenamiento()."<br>";

?>